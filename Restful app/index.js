/*
* primary file for the api.
*
*
*/
const http = require('http')
const url = require('url')
const StringDecoder = require('string_decoder').StringDecoder

//the server should respond to all requests with a string.
var server = http.createServer(function(req, res){
    //get the url and parse it.
    var parseUrl = url.parse(req.url, true) 

    //get the path from the url.
    var path = parseUrl.pathname //path is everything after the port # excludes the qry string.
    var trimmedPath = path.replace(/^\/+|\/+$/g, '')
    
    //get the query string as an object.
    var queryStringObject = parseUrl.query

    //get the http method.
    var method = req.method.toLowerCase() //by default curl uses GET.
    
    //get the headers as an object.
    var headers = req.headers

    //get the payload, if any.
    var decoder = new StringDecoder('utf-8')
    var buffer = ''
    req.on('data', function(data){ //'data' it's an event.
        buffer += decoder.write(data)
    })
    req.on('end', function(){ //'end' it's an event. always called regardless of payload.
        buffer += decoder.end()

        //choose the handler this request should go to. If one is not found, use the notFound handler.
        var chosenHandler = typeof(router[trimmedPath]) !== 'undefined' ? router[trimmedPath] : handlers.notFound

        //build the data object to send to the handler.
        var data = {
            'trimmedPath': trimmedPath,
            'queryStringObject': queryStringObject,
            'method': method,
            'headers': headers,
            'payload': buffer
        }

        //route the request to the handler specified in the router.
        chosenHandler(data, function(statusCode, payload){
            //use the status code called back by the handler or default to 200.
            statusCode = typeof(statusCode) == 'number' ? statusCode : 200
            
            //use the payload called back by the handler or default to an empty object.
            payload = typeof(payload) == 'object' ? payload : {}
        })
        //send the response (new location).
        res.end('Hello World!\n')

        //log the request path (new location).
        console.log('request received with this payload: ',buffer)
    })

    //send the response (original location).
    //res.end('Hello World!\n')

    //log the request path (original location).
    //console.log('request received on path ' + trimmedPath + ' with method: ' + method + ' and with these query string parameters ', queryStringObject)
    //console.log('request received with these headers: ',headers)    
    
})
//start the server, and have it listen on port 3000.
server.listen(3000, function(){
    console.log('the server is listening on port 3000 now')
})

//process to send the request.
//define the handlers.
var handlers = {}

//sample handler.
handlers.sample = function(data, callback){
    //callback a http status code and a payload object.
    callback(406, {'name' : 'sample handler'})
}

//not found handler.
handlers.notFound = function(data, callback){
    callback(404)
}

//define a request router.
var router = {
    'sample' : handlers.sample
}